# slick-crud-app
This is a simple Python CRUD application that uses data (https://people.sc.fsu.edu/~jburkardt/data/csv/addresses.csv) from the Department of Scientific Computing at Florida State University along with Flask and SQLite3 to teach simple MySQL queries.

## Application Notes:
This CRUD application provides MySQL query code for simple database operations performed by the user via a GUI.  These simple database operations are ideal for beginners to create and maintain small databases.  The user can insert data into a table, delete data from a table, edit data in a table, and list data in a table.  When the user performs these operations in the GUI, the corresponding query code is displayed.

This application is meant to be simple for demonstration purposes only.  The goal is to give you a foundation on how to build a Python web application that connects to an SQLite database.  In addition, if you are not familiar with MySQL queries, it will give you a basic understanding of this as well.  It is probably not going to be much useful beyond this purpose.  Hopefully, after reviewing this code you will be on your way to building amazing Flask web applications using SQLite.

## Project Details:
The project structure is as follows:
```
.
├── README.md
├── data
│   └── addresses.csv
├── example.db
├── flask_app.py
├── functions
│   ├── download_data.py
│   ├── insert_data.py
│   └── sqlquery.py
├── requirements.txt
├── run_server.sh
├── set_project.sh
├── templates
    └── sqldatabase.html
```
The `data` directory has a local copy of actual data just to avoide any further download of the data while building the database.

The `example.db` is being used as main database for the application. It is a `SQLite` database which is a flatfile like database and does not require any running server like mysql to query the data. Python `sqlite3` library has direct support for connecting SQLite databases.

The code for utility function are stoerd in the `function` directory, here `download_data` and `insert_data` are used only once to download and prepopulate the datatable at the beginning of the project.

The `sqlquery` has all the functions used by the backend to Read, Create, Update and Delete certain rows from the database. The function are imported into the `flask_app` file and used by the backend server.

The `templates` directory has the frontend HTML template, which is rendered by the backend server and used as tempalte. When user input personal information it receive the data from the form then use `jquery` to send the request to the backend server for processing, when the operation is successful the sql code is returned and showed at the backend.

The `flask_app` has the main backend code for the server. The backend server is created with a python framework called `flask`, which is popular for creating rest API based web application. The flask framework is also capable of rendering HTML template by using another library called `Jinja`. The frontend rendering code is also added as function in this file as a return for each function.

lets see one of the endpoint:
```python
@app.route("/query_edit", methods=["POST", "GET"])  # this is when user clicks edit link
def sql_editlink():
    from functions.sqlquery import sql_query, sql_query2

    if request.method == "GET":
        elname = request.args.get("elname")
        efname = request.args.get("efname")
        eresults = sql_query2(
            """ SELECT * FROM data_table where first_name = ? and last_name = ?""",
            (efname, elname),
        )
    results = sql_query(""" SELECT * FROM data_table""")
    return render_template("sqldatabase.html", eresults=eresults, results=results)
```

When user click the edit link at the frontend for any data, in the backend jquery send the first name and last to the database to collect all other filed form the backend database. The backend flask server receive the input as a argument in `requests.args` variable. The using the `requests.args.get("...")` function it collect the user query and fill up the place holder of the sql function `?` and run a query using the the `sql_query2` function to collect that row from the database only. Other wise it query the whole database to populate the bottom table. When the database query result is ready it use the `render_template` function from `flask` to render the `HTML` template to show the changes.

Other files in the project are optional. The `set_project` can be run as shell command with `./set_project.sh` to set up the initial project
The `run_server` shell file can be run to initialize the backend server with `./run_server.sh`.

So every time when showcasing the project you will run `./run_server.sh` which will start the server and will show the following log:
```
(venv) CrudApp|main⚡ ⇒ ./run_server.sh 
./run_server.sh: line 1: bin/bash: No such file or directory
Initiating Server !
 * Serving Flask app 'flask_app'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 431-812-516

```