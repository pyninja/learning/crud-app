import pandas as pd
import sqlite3
import os


data_url = "data/addresses.csv"
headers = ["first_name", "last_name", "address", "city", "state", "zip"]
data_table = pd.read_csv(data_url, header=None, names=headers, converters={"zip": str})

# Clear example.db if it exists
if os.path.exists("example.db"):
    os.remove("example.db")

# Create a database
conn = sqlite3.connect("example.db")

# Add the data to our database
data_table.to_sql(
    "data_table",
    conn,
    dtype={
        "first_name": "VARCHAR(256)",
        "last_name": "VARCHAR(256)",
        "address": "VARCHAR(256)",
        "city": "VARCHAR(256)",
        "state": "VARCHAR(2)",
        "zip": "VARCHAR(5)",
    },
)

conn.row_factory = sqlite3.Row


if __name__ == "__main__":
    with sqlite3.connect("example.db") as conn:
        test_df = pd.read_sql_query("SELECT * FROM data_table", conn)
        print(test_df.head())
